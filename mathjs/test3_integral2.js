// this is the working version
//import * as math from '/Users/jim.sun/code/node/node_modules/mathjs-simple-integral/node_modules/mathjs/dist/math.js'
const math = require('mathjs')

var plotly = require('plotly')("jimsun", "EEU5lVOS2lLHuxri4sXn")

// example to use math.js
console.log(math.sqrt(-4)); // 2i

math.import(require('mathjs-simple-integral'));

// examples to call the math.integral function
// var theIntegral = math.integral('x^2', 'x', {simplify: false});
// console.log("the Integral of 'x^2' is: " + theIntegral.toString())
// console.log(math.integral('x^2', 'x')); // 'x ^ 3 / 3'
// math.integral('1/x', 'x'); // 'log(abs(x))'
// math.integral('e^x', 'x'); // 'e^x'
// math.integral('cos(2*x+pi/6)', 'x'); // 'sin(2 * x + pi / 6) / 2'

var polynormialString = "4 * sin(x) + 5 * cos(x/2)"
console.log("polynormialString is: " + polynormialString)
const polynormialExpr = math.compile(polynormialString)

// evaluate the polynormialString repeatedly for different values of x
const xValues = math.range(-10, 10, 0.5).toArray()
const yValues = xValues.map(function (x) {
    return polynormialExpr.eval({x: x})
})

// render the plot using plotly
const polynormialTrace = {
    x: xValues,
    y: yValues,
    type: 'scatter',
    name: 'Polynormial',
}

var integralString = math.integral(polynormialString, 'x', {simplify: false});
console.log("integralString is: " + integralString.toString())
const exprIntegral = math.compile(integralString.toString())
const yValuesIntegral = xValues.map(function (x) {
    return exprIntegral.eval({x: x})
})
const integralTrace = {
    x: xValues,
    y: yValuesIntegral,
    type: 'scatter',
    name: 'Integral',
}

const randowmLine = {
    x: [1, 2, 3, 4, 5],
    y: [1, 2, 4, 8, 16]
}

// This sets the title for the plot
function getLayout(plotTitleHint) {
    var layout = {
        title: plotTitleHint,
        xaxis: {
            title: 'x value'
        },
        yaxis: {
            title: 'y value'
        }
    };
    return layout
}

// The two x values for the start and end point of the integral
const firstX = 2
const secondX = 5

var firstYValue = [firstX].map(function (x) {
    return exprIntegral.eval({x: x})
})
console.log("firstYValue[0]=" + firstYValue[0])

var secondYValue = [secondX].map(function (x) {
    return exprIntegral.eval({x: x})
})
console.log("secondYValue[0]=" + secondYValue[0])

// find the start line for the integral
// the points of inflection needs to be calculated using the 2nd derivative of a polynormial
function getIntegralPoints() {
    var integralPoints = []
    // this is the point on the integral graph
    // var integralPoint = func(firstX)
    var integralPoint = firstYValue[0]
    for (y = 0; y <= integralPoint; y += 0.5) {
        //console.log("->adding integralPoint=" + y)
        integralPoints.push(y)
    }
    return integralPoints
}
const integralYValues1 = getIntegralPoints()
console.log('Number of y points: ' + integralYValues1.length)
console.log('max of y points: ' + math.max(integralYValues1))
// put the list of traces of the points of integral point in a list
const integralXValues1 = xValues.map(function (x) {
    return firstX
})
const integralLine1 = {
    x: integralXValues1,
    y: integralYValues1,
    type: 'scatter',
    name: 'startX',
    mode: 'lines',
    text: 'startX',
    title: 'startX',
}

// now find the finish line for the integral
var integralYValues2 = []
var integralPoint2 = secondYValue[0]
for (y = 0; y <= integralPoint2; y += 0.5) {
    integralYValues2.push(y)
}
console.log('Number of y points: ' + integralYValues2.length)
console.log('max of y points: ' + math.max(integralYValues2))
const integralXValues2 = xValues.map(function (x) {
    return secondX
})
const integralLine2 = {
    x: integralXValues2,
    y: integralYValues2,
    type: 'scatter',
    name: 'endX',
    mode: 'lines',
    text: 'startX',
    title: 'startX',
}

const data = [polynormialTrace, integralTrace, integralLine1, integralLine2]

// The visual part of the plot
var layout = {
    title: "Fundamential Theorem of Calculus",
    xaxis: {
        autotick: false,
        ticks: "outside",
        tick0: 0,
        dtick: 0.5,
        ticklen: 8,
        tickwidth: 4,
        tickcolor: "#000",
        title: 'x value'
    },
    yaxis: {
        autotick: false,
        ticks: "outside",
        tick0: 0,
        dtick: 0.5,
        ticklen: 8,
        tickwidth: 4,
        tickcolor: "#000",
        title: 'y value'
    }
};
var graphOptions = {layout: layout, filename: "integral", fileopt: "overwrite"};

plotly.plot(data, graphOptions, function (err, msg) {
    if (err) return console.log(err);
    console.log(msg);
});