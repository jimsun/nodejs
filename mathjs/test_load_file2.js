var http = require('http');
var dt = require('../myfirstmodule');
require('./test3');
const math = require('mathjs')

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("The date and time are currently: " + dt.myDateTime() + "<br/>");

    var iii = math.integral('x^2', 'x', {simplify: false});
    res.write("math.integral=" + iii.toString())

    res.end();
}).listen(8082);
