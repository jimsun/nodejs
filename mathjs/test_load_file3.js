var http = require('http');
var dt = require('../myfirstmodule');
const math = require('mathjs')

// use math.js
console.log(math.sqrt(-4)); // 2i

math.import(require('mathjs-simple-integral'));

console.log("The function is: " + 'x^2')
var iii = math.integral('x^2', 'x', {simplify: false});
console.log("integral is: " + iii.toString())
// alert(iii.toString())

//console.log (math.integral('x^2', 'x')); // 'x ^ 3 / 3'
math.integral('1/x', 'x'); // 'log(abs(x))'
math.integral('e^x', 'x'); // 'e^x'
math.integral('cos(2*x+pi/6)', 'x'); // 'sin(2 * x + pi / 6) / 2'

function testing() {
    console.log("The function is: jimsun" + 'x^2')
}
testing()


http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('\n' +
        '        <html>\n' +
        '        <head>\n' +
        '        <title>math.js | plot</title>\n' +
        '        <script src="https://unpkg.com/mathjs@5.2.3/dist/math.min.js"></script>\n' +
        '        <script src="https://cdn.plot.ly/plotly-1.35.2.min.js"></script>\n' +
        '        <script language="javascript" type="text/javascript" src="./test3.js"></script>\n' +
        '        <style>\n' +
        '        input[type=text] {\n' +
        '        width: 300px;\n' +
        '    }\n' +
        '\n' +
        '    input {\n' +
        '        padding: 6px;\n' +
        '    }\n' +
        '\n' +
        '    body, html, input {\n' +
        '        font-family: sans-serif;\n' +
        '        font-size: 11pt;\n' +
        '    }\n' +
        '\n' +
        '    form {\n' +
        '        margin: 20px 0;\n' +
        '    }\n' +
        '</style>\n' +
        '    </head>\n' +
        '    <body>\n' +
        '    <form id="form">\n' +
        '        <label for="eq">Enter an equation:</label>\n' +
        //'    <input type="text" id="eq" value="1 / (2 + 1) * x ^ (2 + 1)"/>\n' +
        '    <input type="text" id="eq" value="4 * sin(x) + 5 * cos(x/2)"/>\n' +
        '        <input type="submit" value="Draw"/>\n' +
        '        </form>\n' +
        '        <form id="form2">\n' +
        '        <label>Draw First Derivative</label>\n' +
        '    <input type="submit" value="GO"/>\n' +
        '        </form>\n' +
        '        <div id="plot"></div>\n' +
        '        <p>\n' +
        '        Used plot library: <a href="https://plot.ly/javascript/">Plotly</a>\n' +
        '        </p>\n' +
        '        <script>\n' +
        // '        alert(math.integral("x^2", "x", {simplify: false}).toString())\n' +
        '        function draw() {\n' +
        '            try {\n' +
        '                const expression = document.getElementById(\'eq\').value\n' +
        '                const expr = math.compile(expression)\n' +
        '                const xValues = math.range(-10, 10, 0.5).toArray()\n' +
        '                const yValues = xValues.map(function (x) {\n' +
        '                    return expr.eval({x: x})\n' +
        '                })\n' +
        '                const trace1 = {\n' +
        '                    x: xValues,\n' +
        '                    y: yValues,\n' +
        '                    type: \'scatter\'\n' +
        '                }\n' +
        '                const randowmLine = {\n' +
        '                    x: [2, 2, 2, 2, 2],\n' +
        '                    y: [1, 2, 4, 8, 16]\n' +
        '                }\n' +
        '                const randowmLine2 = {\n' +
        '                    x: [5, 5, 5, 5, 5],\n' +
        '                    y: [1, 2, 4, 8, 16]\n' +
        '                }\n' +
        '                const data = [trace1, randowmLine, randowmLine2]\n' +
        '                Plotly.newPlot(\'plot\', data)\n' +
        '            } catch (err) {\n' +
        '                console.error(err)\n' +
        '                alert(err)\n' +
        '            }\n' +
        '        }\n' +
        '\n' +
        '    draw()\n' +
        '    </script>\n' +
        '    </body>\n' +
        '    </html>')

    res.end();
}).listen(8082);
