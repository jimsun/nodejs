// this is the working version
//import * as math from '/Users/jim.sun/code/node/node_modules/mathjs-simple-integral/node_modules/mathjs/dist/math.js'
const math = require('mathjs')

// use math.js
console.log (math.sqrt(-4)); // 2i

math.import(require('mathjs-simple-integral'));


console.log("The function is: " + 'x^2')
var iii = math.integral('x^2', 'x', {simplify: false});
console.log(iii.toString())
//alert(iii.toString())

//console.log (math.integral('x^2', 'x')); // 'x ^ 3 / 3'
math.integral('1/x', 'x'); // 'log(abs(x))'
math.integral('e^x', 'x'); // 'e^x'
math.integral('cos(2*x+pi/6)', 'x'); // 'sin(2 * x + pi / 6) / 2'

function testing() {
    console.log("The function is: jimsun" + 'x^2')
}